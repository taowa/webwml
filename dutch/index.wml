#use wml::debian::mainpage title="Het universele Besturingssysteem" GEN_TIME="yes"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="db28b03ffeea30dd379cb4120e6f83d0e85722d8"

# Last Translation Update by: $Author$
# Last Translation Update at: $Date$

#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"

<span class="download"><a href="<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso">Download Debian <current_release_short><em>(64-bits PC Netwerk-installatie)</em></a> </span>

<div id="splash">
        <h1>Debian</h1>
</div>

<div id="intro">
<p>Debian is een
<a href="intro/free">vrij</a> besturingssysteem (OS) voor uw computer.
Een besturingssysteem is de verzameling van basis- en hulpprogramma's
die er voor zorgen dat uw computer werkt.
</p>

<p>Debian geeft u veel meer dan een besturingssysteem alleen:
het bevat meer dan <packages_in_stable>&nbsp;<a
href="distrib/packages">pakketten</a>, voorgecompileerde programma's in
een handig formaat zodat ze gemakkelijk kunnen worden geïnstalleerd
op uw computer. <a href="intro/about">Meer informatie...</a></p>

</div>

<hometoc/>

<p class="infobar">
De <a href="releases/stable/">meest recente release van Debian</a> is
<current_release_short>. De laatste update aan deze release heeft
plaatsgevonden op <current_release_date>. Lees meer over de
<a href="releases/">beschikbare versies van Debian</a>.</p>


<h2>Beginnen met Debian</h2>
<p>Gebruik de navigatiebalk aan de bovenkant van deze pagina voor meer informatie.</p>
<p>Gebruikers met een andere moedertaal dan Engels, kunnen de sectie <a href="international/">Debian internationaal</a> raadplegen, en personen die
een ander systeem dan Intel x86 gebruiken, wordt aangeraden de sectie <a href="ports/">Ports/Architecturen</a> te raadplegen.</p>
<hr />
<a class="rss_logo" href="News/news">RSS</a>

<h2>Nieuws</h2>

<p><:= get_recent_list('News/$(CUR_YEAR)', '6', '$(ENGLISHDIR)', '', '\d+\w*' ) :></p>

<p>Voor oudere nieuwsberichten, zie de <a
href="$(HOME)/News/">Nieuwspagina</a>.
Als u mail wilt ontvangen met het laatste Debian nieuws kunt u zich
aanmelden voor de <a href="MailingLists/debian-announce">debian-announce
mailinglijst</a>.</p>

<hr />
<a class="rss_logo" href="security/dsa">RSS</a>
<h2>Beveiligingsberichten</h2>

<p><:= get_recent_list ('security/2w', '10', '$(ENGLISHDIR)', 'bydate', '(2000\d+\w+|dsa-\d+)' ) :></p>

<p>Voor oudere beveiligingsberichten, zie de <a
href="$(HOME)/security/">beveiligingspagina</a>. Als u
beveiligingsberichten direct per e-mail wilt ontvangen wanneer ze
verschijnen, kunt u zich aanmelden voor de <a
href="https://lists.debian.org/debian-security-announce/">debian-security-announce
mailinglijst</a>.</p>

{#rss#:
<link rel="alternate" type="application/rss+xml"
 title="Debian-nieuws" href="News/news">
<link rel="alternate" type="application/rss+xml"
 title="Debian Project News" href="News/weekly/dwn">
<link rel="alternate" type="application/rss+xml"
title="Debian Veiligheidsadviezen (enkel de titels)" href="security/dsa">
<link rel="alternate" type="application/rss+xml"
title="Debian Veiligheidsadviezen (samenvattingen)" href="security/dsa-long">
:#rss#}
