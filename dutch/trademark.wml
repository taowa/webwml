#use wml::debian::template title="Debian handelsmerken"
#use wml::debian::toc
#use wml::debian::translation-check translation="ba01cfdc529712e3626bdf15fd37d39e94126794"

<toc-display />


<toc-add-entry name="trademarks">Handelsmerken</toc-add-entry>

<p>Debian is vastbesloten om zijn handelsmerk, zijn logo's en zijn stijlen
te beschermen en een consistent gebruik ervan te verzekeren en het ook
makkelijker te maken voor elke <em>bonafide</em> gebruiker om ze te
gebruiken. Een element van dit proces is dat het handelsmerk Debian in de
Verenigde Staten een geregistreerd handelsmerk is van <a
href="https://www.spi-inc.org/corporate/trademarks/">Software in the Public
Interest, Inc.</a>, beheerd door het Debian project. Met het oog op een
<em>registratie buiten de Verenigde Staten</em>, hebben we een aanvraag
ingediend bij het <a href="http://en.wikipedia.org/wiki/Madrid_system">
Protocol van Madrid</a> om deze bescherming uit te breiden tot de Europese
Unie, China en Japan. Het handelsmerk Debian werd ook geregistreerd
<a href="http://pesquisa.inpi.gov.br/MarcaPatente/jsp/servimg/servimg.jsp?BasePesquisa=Marcas">in Brazilië</a>,
N. do processo 827045310.</p>

<p>Het handelsmerk Debian werd voor de eerste maal <a
href="http://tarr.uspto.gov/servlet/tarr?regser=serial&amp;entry=75386376">geregistreerd</a> op 21 december 1999,
maar het is sinds augustus 1993 in gebruik. Het is geregistreerd onder de
Nice Class 009: <q>Computer Utility and Operating System Software</q>
(software voor computerhulpprogramma en besturingssysteem).</p>


<toc-add-entry name="policy">Handelsmerkbeleid</toc-add-entry>

<pre><code>Versie: 2.0
Gepubliceerd: 19 januari 2013
</code></pre>

<p><a href="https://www.spi-inc.org/">Software in the Public Interest, Inc.</a>
is eigenaar van een aantal handelsmerken in verbale en logo-vorm, met
inbegrip van merken, slogans en stijlen. Dit beleid betreft alle merken, in
verbale en logo-vorm, gezamenlijk aangeduid als <q>Debian handelsmerken</q>.
U vindt een niet-exhaustieve lijst van handelsmerken van Debian, waaronder
zowel geregistreerde als niet-geregistreerde (maar anderszins wettelijk
erkende) handelsmerken op onze
<a href="#trademarks">handelsmerkenpagina</a>.</p>

<p>Het doel van dit hansdelsmerkbeleid is:</p>

<ol>
  <li>een wijdverspreid gebruik en aanvaarding van de handelsmerken van Debian aanmoedigen,</li>
  <li>verduidelijking brengen over wat een passend gebruik van de handelsmerken van Debian door derden is,</li>
  <li>voorkomen dat de handelsmerken van Debian misbruikt worden, wat voor
gebruikers verwarrend of misleidend kan zijn waar het Debian en zijn
filialen betreft.</li>
</ol>

<p>Merk op dat het niet de bedoeling van dit beleid is om commerciële
activiteiten rondom Debian te beperken. We moedigen bedrijven aan om op
Debian te werken als daarbij voldaan wordt aan deze beleidsrichtlijnen.</p>

<p>Hierna volgen de richtlijnen voor een passend gebruik van de handelsmerken
van Debian door uitgevers en andere derden. Elk gebruik van of verwijzing
naar de handelsmerken van Debian dat niet in overeenstemming is met deze
richtlijnen, of enig ander ongeoorloofd gebruik van of verwijzing naar de
handelsmerken van Debian, of het gebruik van merken die een verwarrende
gelijkenis vertonen met de handelsmerken van Debian, is verboden en kan de
rechten van Debian op zijn handelsmerken schenden.</p>

<p>Elk gebruik van de handelsmerken van Debian op een misleidende en
bedrieglijke wijze of op een wijze die Debian in diskrediet brengt, zoals
leugenachtige reclame, is altijd verboden.</p>


<h3>In welke gevallen u de handelsmerken van Debian mag gebruiken zonder toestemming te vragen</h3>

<ol>

  <li>U mag de handelsmerken van Debian gebruiken om feitelijk ware
uitspraken te doen over Debian of om waarheidsgetrouw te communiceren over
compatibiliteit met uw product.</li>

  <li>Als het beoogde gebruik van de handelsmerken van Debian omschreven kan
worden als <q>redelijk nominatief gebruik</q>, d.w.z. dat u in een tekst
louter aangeeft dat u het over Debian heeft, zonder een suggestie van
sponsoring of goedkeuring te doen.</li>

  <li>U kunt de handelsmerken van Debian gebruiken om uw diensten of
producten in verband met Debian te beschrijven of er reclame voor te maken op
een niet-misleidende wijze.</li>

  <li>U kunt de handelsmerken van Debian gebruiken om een beschrijving te
geven van Debian in artikels, titels of blogs.</li>

  <li>U kunt met de handelsmerken van Debian t-shirts maken,
bureaubladachtergronden, petten en andere koopwaar voor <em>niet-commercieel
gebruik</em>.</li>

  <li>U kunt met de handelsmerken van Debian ook koopwaar voor <em>commercieel
gebruik</em> maken. In geval van commercieel gebruik, raden we aan dat u
klanten waarheidsgetrouw aankondigt welk deel van de verkoopprijs in
voorkomend geval aan het Debian project geschonken zal worden. Raadpleeg onze
<a href="$(HOME)/donations">pagina over donaties</a> voor meer informatie over hoe u donaties kunt doen aan het Debian project.</li>

</ol>


<h3>In welke gevallen u de handelsmerken van Debian NOOIT mag gebruiken
zonder toestemming te vragen</h3>

<ol>

  <li>U kunt de handelsmerken van Debian op geen enkele manier gebruiken die
de suggestie inhoudt van verbondenheid met of ondersteuning door het Debian
project of de Debian gemeenschap, wanneer dit niet met de waarheid strookt</li>

  <li>U kunt de handelsmerken van Debian niet gebruiken in de naam van een
bedrijf of organisatie of als naam voor een product of dienst.</li>

  <li>U mag geen naam gebruiken die een verwarrende gelijkenis vertoont met
de handelsmerken van Debian.</li>

  <li>U mag geen handelsmerken van Debian gebruiken in een domeinnaam, al dan
niet met commerciële bedoelingen.</li>

</ol>


<h3>Hoe u de handelsmerken van Debian moet gebruiken</h3>

<ol>

  <li>Gebruik de handelsmerken van Debian op een wijze die duidelijk maakt
dat uw project verband houdt met het Debian project, maar dat het geen
onderdeel van Debian is, niet geproduceerd wordt door het Debian project en
niet gesteund wordt door het Debian project.</li>

  <li>Vermeldt duidelijk dat Software in the Public Interest, Inc. eigenaar is van de handelsmerken van Debian.

    <p><em>Voorbeeld:</em></p>

    <p>[HANDELSMERK] is een (<q>geregistreerd,</q> wanneer van toepassing)
handelsmerk dat eigendom is van Software in the Public Interest, Inc.</p>
  </li>

  <li>Voeg op uw website en bij alle aanverwante gedrukte materialen een
verklaring toe waarin u ontkent dat er sprake kan zijn van enige vorm van
sponsoring door, betrokkenheid bij of goedkeuring door Debian.

    <p><em>Voorbeeld:</em></p>

    <p>PROJECT X is niet verbonden met Debian. Debian is een geregistreerd
    handelsmerk dat eigendom is van Software in the Public Interest, Inc.</p>
  </li>

  <li>Maak een onderscheid tussen de handelsmerken van Debian en de woorden
die in de buurt ervan staan, door ze schuin, vet of onderstreept weer te
geven.</li>

  <li>Gebruik de handelsmerken van Debian in hun exacte vorm, niet afgekort
of gesplitst en niet in combinatie met een ander woord / andere woorden.</li>

  <li>Maak geen acroniemen met de handelsmerken van Debian.</li>

</ol>


<h3>Toestemming voor gebruik</h3>

<p>Wanneer u onzeker bent over het gebruik van de handelsmerken van Debian,
of om toestemming te vragen voor een gebruik dat door deze richtlijnen niet
toegestaan is, moet u een e-mail sturen (in het Engels) naar
<a href="mailto:trademark@debian.org?subject=Trademark%20Use%20Request">\
trademark@debian.org met als onderwerp <q>Trademark Use Request</q></a>; zorg ervoor dat u in de tekst van uw bericht de volgende informatie verstrekt:</p>
<ul>
<li>Naam van de gebruiker</li>
<li>Name van de organisatie / het project</li>
<li>Doel van het gebruik (commercieel/niet-commercieel)</li>
<li>Aard van het gebruik</li>
</ul>


<h3>Recentere versies van deze beleidsrichtlijnen</h3>

<p>Deze beleidsrichtlijnen kunnen van tijd tot tijd herzien worden en bijgewerkte versies zullen beschikbaar zijn op <url https://www.debian.org/trademark>.</p>


<h3>Richtlijnen voor het gebruik van logo's</h3>

<ul>

  <li>Elke schaalverandering moet de originele proporties van het logo
behouden.

  <li>Gebruik de logo's van Debian niet als onderdeel van het logo van uw
bedrijf of uw product of van uw merk zelf. Ze mogen gebruikt worden als
onderdeel van een pagina met de beschrijving van uw producten en diensten.

  <li>U dient ons niet om toestemming te vragen voor het gebruik van de
logo's op uw eigen website, enkel en alleen als hyperlink naar de website van
het Debian project.

</ul>

<p>
Voor alle vragen in verband met deze beleidsrichtlijnen kunt u een e-mail
sturen naar <email "trademark@debian.org">.
</p>

<toc-add-entry name="licenses">Organisaties die een licentie hebben om de
handelsmerken van Debian te gebruiken</toc-add-entry>

<p>De volgende organisaties hebben via een licentieovereenkomst een licentie
verkregen voor het gebruik van het handelsmerk Debian:</p>

<ul>

<li>Stiftelsen SLX Debian Labs, bij SPI-besluit
<a href="https://www.spi-inc.org/corporate/resolutions/2004/2004-01-05.bmh.1/">\
2004-01-05.bmh.1</a>.</li>

<li>Vincent Renardias en <q>Les logiciels du soleil</q>, bij SPI-besluit
<a href="https://www.spi-inc.org/corporate/resolutions/1999/1999-08-06.mgs/">\
1999-08-06.mgs</a>.</li>

<li>
debian.ch, <a
href="https://lists.debian.org/debian-www/2011/04/msg00163.html">\
door Stefano Zacchiroli</a>, Debian Projectleider in 2011.
</li>

</ul>
