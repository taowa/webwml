<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The support and utility classes used by the Rails 3.2 framework allow
remote attackers to cause a denial of service (SystemStackError) via a
large XML document depth.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.2.6-6+deb7u2.</p>

<p>Additionally this upload adds 'active_support/security_utils' that will
be used by ruby-actionpack-3.2 to address <a href="https://security-tracker.debian.org/tracker/CVE-2015-7576">CVE-2015-7576</a>.</p>

<p>We recommend that you upgrade your ruby-activesupport-3.2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-603.data"
# $Id: $
