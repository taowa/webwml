<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Marcin Noga discovered a buffer overflow in the JPEG loader of the GDK
Pixbuf library, which may result in the execution of arbitrary code if
a malformed file is opened.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.26.1-1+deb7u6.</p>

<p>We recommend that you upgrade your gdk-pixbuf packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1100.data"
# $Id: $
