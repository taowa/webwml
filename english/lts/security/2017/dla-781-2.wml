<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Brad Barnett found that the recent security update of Asterisk could
cause immediate SIP termination due to an incomplete fix for
<a href="https://security-tracker.debian.org/tracker/CVE-2014-2287">CVE-2014-2287</a>.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1:1.8.13.1~dfsg1-3+deb7u6.</p>

<p>We recommend that you upgrade your asterisk packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-781-2.data"
# $Id: $
