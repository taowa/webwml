<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several issues were corrected in nagios3, a monitoring and management
system for hosts, services and networks.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18245">CVE-2018-18245</a>

      <p>Maximilian Boehner of usd AG found a cross-site scripting (XSS)
      vulnerability in Nagios Core. This vulnerability allows attackers
      to place malicious JavaScript code into the web frontend through
      manipulation of plugin output. In order to do this the attacker
      needs to be able to manipulate the output returned by nagios
      checks, e.g. by replacing a plugin on one of the monitored
      endpoints. Execution of the payload then requires that an
      authenticated user creates an alert summary report which contains
      the corresponding output.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9566">CVE-2016-9566</a>

      <p>It was discovered that local users with access to an account in
      the nagios group are able to gain root privileges via a symlink
      attack on the debug log file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-1878">CVE-2014-1878</a>

      <p>An issue was corrected that allowed remote attackers to cause a
      stack-based buffer overflow and subsequently a denial of service
      (segmentation fault) via a long message to cmd.cgi.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2013-7205">CVE-2013-7205</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2013-7108">CVE-2013-7108</a>

      <p>A flaw was corrected in Nagios that could be exploited to cause a
      denial-of-service. This vulnerability is induced due to an
      off-by-one error within the process_cgivars() function, which can
      be exploited to cause an out-of-bounds read by sending a
      specially-crafted key value to the Nagios web UI.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
3.5.1.dfsg-2+deb8u1.</p>

<p>We recommend that you upgrade your nagios3 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1615.data"
# $Id: $
