<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A XML External Entity (XXE) vulnerability was discovered in c3p0, a
library for JDBC connection pooling, that may be used to resolve
information outside of the intended sphere of control.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
0.9.1.2-9+deb8u1.</p>

<p>We recommend that you upgrade your c3p0 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1621.data"
# $Id: $
