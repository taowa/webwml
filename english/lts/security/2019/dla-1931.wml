<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a ECDSA timing attack in the libgcrypt20 cryptographic library.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13627">CVE-2019-13627</a>

    <p>ECDSA timing attack</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.6.3-2+deb8u6.</p>

<p>We recommend that you upgrade your libgcrypt20 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1931.data"
# $Id: $
