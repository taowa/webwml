#use wml::debian::translation-check translation="b38dc0414f704da70b73641cb9b740bf594d5f2b" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il existait plusieurs vulnérabilités de double libération de zone de mémoire
dans python-psutil, un module de Python fournissant des fonctions pratiques pour
accéder aux données de processus système.</p>

<p>Cela était dû à une gestion incorrecte du compte de références à l’intérieur
de boucles for ou while qui convertissaient les données système en des objets
Python.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18874">CVE-2019-18874</a>

<p>La fonction psutil (c'est-à-dire, python-psutil) jusqu’à 5.6.5 pouvait
contenir une double libération de zone mémoire. Cela se produisait à cause
d’une gestion incorrecte de refcount à l’intérieur d’une boucle within ou for
qui convertissait des données système en un objet Python.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 2.1.1-1+deb8u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python-psutil.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1998.data"
# $Id: $
