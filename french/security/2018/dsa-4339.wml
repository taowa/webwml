#use wml::debian::translation-check translation="c1763bbf92b379a36ce63d2b8ac86ad7b4c6a7ff" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans Ceph, un système
distribué de stockage et de fichiers : le protocole d'authentification
cephx était vulnérable à des attaques par rejeu et calculait incorrectement
les signatures, <q>ceph mon</q> ne validait pas les capacités pour les
opérations sur les espaces de stockage (« pool ») (avec pour conséquence la
potentielle corruption ou suppression d'images d'instantané) et une
vulnérabilité de chaîne de formatage dans libradosstriper pourrait avoir
pour conséquence un déni de service.</p>

<p>Pour la distribution stable (Stretch), ces problèmes ont été corrigés
dans la version 10.2.11-1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ceph.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de ceph, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/ceph">\
https://security-tracker.debian.org/tracker/ceph</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2018/dsa-4339.data"
# $Id: $
