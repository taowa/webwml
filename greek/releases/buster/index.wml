#use wml::debian::template title="Πληροφορίες της έκδοσης του Debian &ldquo;buster&rdquo;"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/buster/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="1f80737f52428a54019a0938a77cafbbf0f97c2a" maintainer="galaxico"


<if-stable-release release="buster">

<p>Το Debian <current_release_buster> κυκλοφόρησε στις 
<current_release_date_buster>.
<ifneq "10.0" "<current_release>"
  "Το Debian 10.0 κυκλοφόρησε αρχικά στις <:=spokendate('2019-07-06'):>."
/>
Η έκδοση περιλαμβάνει πολλές μεγάλες αλλαγές οι οποίες περιγράφονται στο <a 
href="$(HOME)/News/2019/20190706">Δελτίο Τυπου</a> και τις <a 
href="releasenotes">Σημειώσεις της έκδοσης</a>.</p>

<p>Για την απόκτηση και εγκατάσταση του Debian, δείτε
τη σελίδα <a href="debian-installer/">πληροφορίες εγκατάστασης</a> και
τον <a href="installmanual">Οδηγό Εγκατάστασης</a>. Για να κάνετε αναβάθμιση
από μια παλιότερη έκδοση του Debian, δείτε τις οδηγίες στις 
<a href="releasenotes">Σημειώσεις της έκδοσης</a>.</p>

<p>Οι ακόλουθες αρχιτεκτονικές υπολογιστών υποστηρίζονται σ' αυτή την 
έκδοση:</p>

<ul>
<:
foreach $arch (@arches) {
	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>

<p>Παρά τις επιθυμίες μας, μπορεί να υπάρχουν μερικά προβλήματα στην παρούσα 
έκδοση, να και έχει ανακηρυχθεί <em>σταθερή</em>. 
Έχουμε κάνει
<a href="errata">μια λίστα με τα σημαντικότερα γνωστά προβλήματα</a>, και 
μπορείτε πάντα να
<a href="reportingbugs">αναφέρετε οποιαδήποτε άλλα ζητήματα</a> σε μας.</p>

<p>Τέλος, αλλά όχι με λιγότερη σημασία, έχουμε δημιουργήσει μια λίστα των <a 
href="credits">ανθρώπων που κερδίζουν την αναγνώριση</a> για την πραγματοποίηση
αυτής της έκδοσης.</p>
</if-stable-release>

<if-stable-release release="stretch">

<p>Η κωδική ονομασία για την επόμενη μείζονα έκδοση του Debian μετά την <a
href="../stretch/">stretch</a> είναι <q>buster</q>.</p>

<p>Αυτή η έκδοση ξεκίνησε σαν ένα αντίγραφο της stretch, και είναι προς το 
παρόν σε μια κατάσταση που ονομάζεται
 <q><a 
href="$(DOC)/manuals/debian-faq/ch-ftparchives#s-testing">δοκιμαστική</a></q>.
Αυτό σημαίνει ότι δεν θα έπρεπε να υπάρχουν προβλήματα τόσο άσχημα όσο με την 
ασταθή ή την πειραματική διανονή, επειδή τα πακέτα επιτρέπεται να μπουν σ' αυτή
τη διανομή μόνο μετά από το πέρασμα συγκεκριμένου χρονικού διαστήματος, και 
εφόσον δεν έχουν καταγραφεί γι' αυτά σφάλματα σοβαρότητας κρίσιμης για την 
έκδοση (release-critical bugs).</p>

<p>Παρακαλούμε, σημειώστε ότι οι αναβαθμίσεις ασφαλείας για την 
<q>δοκιμαστική</q> διανομή <strong>δεν</strong> τις διαχειρίζεται ακόμα η 
ομάδα ασφαλείας. Ως εκ τούτου, η <q>δοκιμαστική</q> διανομή 
<strong>δεν</strong> έχει αναβαθμίσεις ασφαλείας με έναν έγκαιρο τρόπο.
# For more information please see the
# <a href="https://lists.debian.org/debian-testing-security-announce/2008/12/msg00019.html">announcement</a>
# of the Testing Security Team.
Σας ενθαρρύνουμε να αλλάξετε προς το παρόν τις επιλογές για το αρχείο πηγών σας
sources.list από την testing στην stretch αν χρειάζεστε πραγματικά υποστήριξη 
ασφαλείας.Δείτε επίσης τα σχετικά στοιχεία στις συχνές ερωτήσεις
<a href="$(HOME)/security/faq#testing">Security Team's FAQ</a> της ομάδας 
ασφαλείας για τη <q>δοκιμαστική</q> διανομή.</p>

<p>Ίσως υπάρχει διαθέσιμο ένα <a href="releasenotes">προσχέδιο των 
σημειώσεων της έκδοσης</a>. Παρακαλούμε, επίσης, <a 
href="https://bugs.debian.org/release-notes">ελέγξτε τις προτεινόμενες 
προσθήκες στις σημειώσεις της έκδοσης</a>.</p>

<p>Για εικόνες εγκατάστασης και τεκμηρίωση σχετικά με την εγκατάσταση της 
<q>testing</q>,δείτε τη σελίδα του <a 
href="$(HOME)/devel/debian-installer/">Εγκαταστάτη του Debian</a>.</p>

<p>Για να βρείτε περισσότερα σχετικά με το πώς δουλεύει η <q>δοκιμαστική</q> 
διανομή, ελέγξτε τη σελίδα 
<a href="$(HOME)/devel/testing">με πληροφορίες των προγραμματιστών σχετικά μ' 
αυτήν</a>.</p>

<p>Ο κόσμος ρωτά συχνά αν υπάρχει μοναδικός <q>μετρητής προόδου</q> μιας 
διανομής. Δυστυχώς, κάτι τέτοιο δεν υπάρχει αλλά μπορούμε να σας παραπέμψουμε 
σε αρκετά σημεία που περιγράφουν πράγματα απαραίτητα για την πραγματοποίηση 
της κυκλοφορίας μιας έκδοσης:</p>

<ul>
  <li><a href="https://release.debian.org/">Generic release status page</a></li>
  <li><a href="https://bugs.debian.org/release-critical/">Release-critical bugs</a></li>
  <li><a href="https://udd.debian.org/bugs.cgi?base=only&amp;rc=1">Base system bugs</a></li>
  <li><a href="https://udd.debian.org/bugs.cgi?standard=only&amp;rc=1">Bugs in standard and task packages</a></li>
</ul>

<p>Επιπρόσθετα, αναφορές της γενικής κατάστασης αναρτώνται από τον διαχειριστή 
της κυκλοφορίας της έκδοσης στη λίστα αλληλογραφίας <a 
href="https://lists.debian.org/debian-devel-announce/">\
debian-devel-announce</a>.</p>

</if-stable-release>
